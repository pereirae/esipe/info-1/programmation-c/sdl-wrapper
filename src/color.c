#include "../include/color.h"

SDL_Color *create_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
    SDL_Color *color = NULL;
    if ((color = (SDL_Color*)malloc(sizeof(SDL_Color))) == NULL) {
        return NULL;
    }
    color->r = r;
    color->g = g;
    color->b = b;
    color->a = a;
    return color;
}

void destroy_color(SDL_Color *color) {
    free(color);
}

void getCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color) {
    SDL_GetRenderDrawColor(renderer, &color->r, &color->g, &color->b, &color->a);
}

void setCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color) {
    SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, color->a);
}
