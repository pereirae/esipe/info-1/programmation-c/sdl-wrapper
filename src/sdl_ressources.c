#include "../include/sdl_ressources.h"

SDL_ressources *create_SDL_ressources(char *w_title, int w_width, int w_height) {
    SDL_ressources *sdl_ressources = NULL;
    if ((sdl_ressources = malloc(sizeof(SDL_ressources))) == NULL) {
        fprintf(stderr, "Errorr occured while allocating memory for the SDL ressources\n");
        return NULL;
    }
    sdl_ressources->window = SDL_CreateWindow(w_title,
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        w_width, w_height,
        SDL_WINDOW_SHOWN);
    if (sdl_ressources->window == NULL) {
        fprintf(stderr, "Error occured while creating the window : %s\n", SDL_GetError());
        return NULL;
    }
    sdl_ressources->renderer = SDL_CreateRenderer(sdl_ressources->window, -1, SDL_RENDERER_PRESENTVSYNC |
                                                                              SDL_RENDERER_ACCELERATED);
    if (sdl_ressources->renderer == NULL) {
        fprintf(stderr, "Error occured while creating the renderer: %s\n", SDL_GetError());
        return NULL;
    }
    return sdl_ressources;
}

void get_current_render_color(SDL_Renderer *renderer, SDL_Color *color) {
    SDL_GetRenderDrawColor(renderer, &color->r, &color->g, &color->b, &color->a);
}

void set_current_render_color(SDL_Renderer *renderer, SDL_Color *color) {
    SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, color->a);
}

void destroy_SDL_ressources(SDL_ressources *sdl_ressources) {
    if (sdl_ressources->renderer != NULL) {
        SDL_DestroyRenderer(sdl_ressources->renderer);
    }
    if (sdl_ressources->window != NULL) {
        SDL_DestroyWindow(sdl_ressources->window);
    }
    free(sdl_ressources);
}
