#include "../include/shape.h"

void drawOuterCircle(SDL_Renderer *renderer, Coord *pos, int radius) {
    int offsetX = 0;
    int offsetY = radius;
    int m = 5 - 4 * radius;
    int x = pos->x;
    int y = pos->y;

    while (offsetY >= offsetX) {
        SDL_RenderDrawPoint(renderer, offsetX + x, offsetY + y);
        SDL_RenderDrawPoint(renderer, offsetX + x, -offsetY + y);
        SDL_RenderDrawPoint(renderer, offsetY + x, offsetX + y);
        SDL_RenderDrawPoint(renderer, offsetY + x, -offsetX + y);
        SDL_RenderDrawPoint(renderer, -offsetX + x, offsetY + y);
        SDL_RenderDrawPoint(renderer, -offsetX + x, -offsetY + y);
        SDL_RenderDrawPoint(renderer, -offsetY + x, -offsetX + y);
        SDL_RenderDrawPoint(renderer, -offsetY + x, offsetX + y);

        if (m > 0) {
            offsetY--;
            m -= 8 * offsetY;
        }
        offsetX++;
        m += 8 * offsetX + 4;
    }
}

void fillCircle(SDL_Renderer *renderer, Coord *pos, int radius) {
    int offsetX = 0;
    int offsetY = radius;
    int m = 5 - 4 * radius;
    int x = pos->x;
    int y = pos->y;

    while (offsetY >= offsetX) {
        SDL_RenderDrawLine(renderer, offsetX + x, -offsetY + y, offsetX + x, offsetY + y);
        SDL_RenderDrawLine(renderer, offsetY + x, offsetX + y, offsetY + x, -offsetX + y);
        SDL_RenderDrawLine(renderer, -offsetX + x, offsetY + y, -offsetX + x, -offsetY + y);
        SDL_RenderDrawLine(renderer, -offsetY + x, -offsetX + y, -offsetY + x, offsetX + y);

        if (m > 0) {
            offsetY--;
            m -= 8 * offsetY;
        }
        offsetX++;
        m += 8 * offsetX + 4;
    }
}

void drawCircle(SDL_Renderer *renderer, Coord *pos, int radius) {
    drawOuterCircle(renderer, pos, radius);
    fillCircle(renderer, pos, radius);
}

void drawRect(SDL_Renderer *renderer, Coord *pos, int height, int width) {
    SDL_Rect rect;

    if (renderer == NULL || pos == NULL) {
        return;
    }

    rect.x = pos->x; rect.y = pos->y; rect.h = height; rect.w = width;
    SDL_RenderDrawRect(renderer, &rect);
}

void drawFilledRect(SDL_Renderer *renderer, Coord *pos, int height, int width) {
    SDL_Rect rect;

    if (renderer == NULL || pos == NULL) {
        return;
    }

    rect.x = pos->x; rect.y = pos->y; rect.h = height; rect.w = width;
    SDL_RenderFillRect(renderer, &rect);
}
