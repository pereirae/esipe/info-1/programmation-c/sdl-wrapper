#include "../include/sdl_wrapper.h"

SDL_ressources *setup_SDL(char *w_title, int w_height, int w_width) {
    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        fprintf(stderr, "Error occured while initialising the SDL : %s\n", SDL_GetError());
        return NULL;
    }

    if (TTF_Init() == -1) {
        fprintf(stderr, "Error occured while initialising the TTF module : %s\n", SDL_GetError());
        return NULL;
    }
    return create_SDL_ressources(w_title, w_width, w_height);
}

void exit_SDL(SDL_ressources *sdl_ressources) {
    destroy_SDL_ressources(sdl_ressources);
    TTF_Quit();
    SDL_Quit();
}
