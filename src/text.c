#include "../include/text.h"

Text *create_text(char *value, TTF_Font *font, SDL_Color color, SDL_Renderer *renderer) {
    Text *text = NULL;
    if ((text = (Text*)malloc(sizeof(Text))) == NULL) {
        return NULL;
    }
    if ((text->surface = TTF_RenderText_Solid(font, value, color)) == NULL) {
        free(text);
        return NULL;
    }
    if ((text->texture = SDL_CreateTextureFromSurface(renderer, text->surface)) == NULL) {
        SDL_FreeSurface(text->surface);
        free(text);
        return NULL;
    }
    return text;
}

void destroy_text(Text *text) {
    SDL_DestroyTexture(text->texture);
    SDL_FreeSurface(text->surface);
    free(text);
}
