#include <time.h>

#include "include/sdl_wrapper.h"
#include "include/SDL2/SDL.h"
#include "include/SDL2/SDL_image.h"

int delay(int ms, SDL_ressources *sdl_ressources) {
    SDL_bool waiting;
    clock_t start;
    clock_t now;
    clock_t wait_duration;
    waiting = SDL_TRUE;
    start = clock();
    while (waiting) {
        while (SDL_PollEvent(&sdl_ressources->event)) {
            switch(sdl_ressources->event.type) {
                case SDL_QUIT:
                    waiting = SDL_FALSE;
                    return 0;
                    break;
                default:
                    break;
            }
        }
        now = clock();
        wait_duration = (now - start) * 1000 / CLOCKS_PER_SEC;
        if (wait_duration >= ms) {
            waiting = SDL_FALSE;
        }
    }
    return 1;
}

int main (int argc, char **argv) {
    SDL_ressources *sdl_ressources = NULL;
    Coord *pos;
    SDL_Color *color;
    SDL_Surface *imgS;
    SDL_Texture *texture = NULL;

    if ((sdl_ressources = setup_SDL("Test", 400, 600)) == NULL) {
        return 1;
    }
    /*pos = create_coord(0, 0);
    color = create_color(255, 0, 0, 255);
    SDL_RenderClear(sdl_ressources->renderer);
    setCurrentRenderColor(sdl_ressources->renderer, color);
    drawRect(sdl_ressources->renderer, pos, 100, 100);
    SDL_RenderPresent(sdl_ressources->renderer);*/

    TTF_Font *font = TTF_OpenFont("assets/fonts/Roboto-Light.ttf", 25);
    SDL_Color color2 = { 255, 0, 0 };
    Text *text = create_text("test", font, color2, sdl_ressources->renderer);
    SDL_Rect rect = {0, 0, 50, 50};

    imgS = IMG_Load("kitten.png");
    texture = SDL_CreateTextureFromSurface(sdl_ressources->renderer, imgS);

    SDL_FreeSurface(imgS);
    SDL_RenderCopy(sdl_ressources->renderer, text->texture, NULL, &rect);
    rect.x = 100;
    SDL_RenderCopy(sdl_ressources->renderer, texture, NULL, &rect);
    SDL_RenderPresent(sdl_ressources->renderer);
    delay(5000, sdl_ressources);
    destroy_text(text);
    SDL_DestroyTexture(texture);
    exit_SDL(sdl_ressources);
    return 0;
}