# SDL Wrapper

## Commande make disponnible :
- make : Compile la bibliothèque
- make clean : Supprime les fichiers .o
- make fclean : Supprime les fichiers .o et la bibliothèque
- make re : Supprimer les fichiers .o, la bibliothèque et compile la bibliothèque
- make test : Compile un exécutable de test faisant appele à des fonctions de la bibliothèque