#ifndef SDL_RESSOURCES_h_
#define SDL_RESSOURCES_h_

#include "SDL2/SDL.h"

/**
 * SDLRessources est une structure qui contient les données 
 * dont la SDL à besoin pour travailler.
 * 
 * \param window La structure représentant la fenêtre de l'application.
 * \param renderer La strucutre qui permet de dessiner dans la fenêtre.
 * \param event La structure qui capture les évenements  (clavier, souris, etc.).
 */
typedef struct SDL_ressources {
    SDL_Window      *window;
    SDL_Renderer    *renderer;
    SDL_Event       event;
} SDL_ressources;

/**
 * Initialise les variables d'une structure SDL_ressources (SDL_Window et SDL_Renderer).
 * 
 * \param w_title The title of the window to be created
 * \param w_height The height of the window to be created
 * \param w_width The width of the window to be created
 * 
 * \returns Un pointeur sur la strcuture SDL_ressources en cas de succès ou NULL en cas d'échec.
 */
SDL_ressources  *create_SDL_ressources(char *w_title, int w_width, int w_height);

/**
 * Détruit proprement les ressources contenues dans une structure sdlRessources.
 * 
 * \param sdl_ressources la structure dont les ressources doivent être détruites.
 */
void destroy_SDL_ressources(SDL_ressources *sdl_ressources);

#endif /* SDL_RESSOURCES */
