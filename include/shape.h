#ifndef SHAPE_h_
#define SHAPE_h_

#include "SDL2/SDL.h"

#include "coord.h"

/* ============ CIRCLES ============ */

/**
 * Trace un cercle de rayon radius à avec le point {x,y} comme point central.
 * Le tracé est effectué avec l'algorithme de tracé de cercle de Bresenham.
 *
 * \param renderer Le renderer SDL permettant de tracer le cercle.
 * \param pos Les coordonées du point au centre du cercle
 * \param radius Le rayon du cercle à tracer.
 */
void drawOuterCircle(SDL_Renderer *renderer, Coord *pos, int radius);

/**
 * Rempli un cercle de rayon radius dont le centre est aux coordonées {x,y}.
 *
 * \param renderer Le renderer SDL permettant de tracer le cercle.
 * \param pos Les coordonées du point au centre du cercle
 * \param radius Le rayon du cercle à tracer.
 */
void fillCircle(SDL_Renderer *renderer, Coord *pos, int radius);

/**
 * Trace un cercle rempli de rayon radius dont le centre est {x,y}.
 *
 * \param renderer Le renderer SDL permettant de tracer le cercle.
 * \param pos Les coordonées du point au centre du cercle
 * \param radius Le rayon du cercle à tracer.
 */
void drawCircle(SDL_Renderer *renderer, Coord *pos, int radius);

/* ============ RECTANGLES ============ */

/**
 * \brief Dessine une rectangle à partir du point pos.
 *
 * \param renderer  Le renderer SDL permettant de tracer le cercle.
 * \param pos       La coordonées du point supérieur gauche du rectangle à dessiner.
 * \param height     La hauteur du rectangle à dessiner.
 * \param width    La largeur du rectangle à dessiner.
 */
void drawRect(SDL_Renderer *renderer, Coord *pos, int height, int width);

/**
 * \brief Dessine une rectangle à partir du point pos.
 *
 * \param renderer  Le renderer SDL permettant de tracer le cercle.
 * \param pos       La coordonées du point supérieur gauche du rectangle à dessiner.
 * \param heiht     La hauteur du rectangle à dessiner.
 * \param width     La largeur du rectangle à dessiner.
 */
void drawFilledRect(SDL_Renderer *renderer, Coord *pos, int height, int width);

#endif /* SHAPE_h_ */
