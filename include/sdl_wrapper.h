#ifndef SDL_WRAPPER_h_
#define SDL_WRAPPER_h_

#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"
#include "sdl_ressources.h"
#include "shape.h"
#include "color.h"
#include "coord.h"
#include "text.h"


/**
 * Setup the SDL by initialising it and creating the associated ressources.
 *
 * @param w_title The title of the window to be created
 * @param w_height The height of the window to be created
 * @param w_width The width of the window to be created
 * @return SDL_ressources* A pointer on the SDL_ressources structure used to manipulate the SDL
 */
SDL_ressources *setup_SDL(char *w_title, int w_height, int w_width);

/**
 * Free the ressources used by the SDL and cleanly shutdown the SDL.
 *
 * \param sdl_ressources A pointer on the SDL_ressources structure used by the program
 */
void exit_SDL(SDL_ressources *sdl_ressources);

#endif
