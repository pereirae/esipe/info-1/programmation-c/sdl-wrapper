#ifndef COORD_h_
#define COORD_h_

#include <stdlib.h>

/**
 * Représente une coordonnée
 *
 * \param x La valeur en X de la coordonnée
 * \param y La valeur en Y de la coordonnée
 */
typedef struct _coord {
    int x;
    int y;
} Coord;

/**
 * Initialise une structure Coord et renvoie un pointeur
 *
 * @param x La valeur en X de la coordonnée
 * @param y La valeur en Y de la coordonnée
 * @return Coord* Un pointeur sur la coordonnée
 */
Coord *create_coord(int x, int y);

/**
 * Détruit une coordonnée.
 *
 * @param coord Le pointeur vers la coordonnée à détruire.
 */
void destroy_coord(Coord *coord);

#endif
