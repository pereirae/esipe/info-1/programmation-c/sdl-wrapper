#ifndef TEXT_h_
#define TEXT_h_

#include "sdl_wrapper.h"

typedef struct text {
    SDL_Surface *surface;
    SDL_Texture *texture;
} Text;

Text *create_text(char *value, TTF_Font *font, SDL_Color color, SDL_Renderer *renderer);

void destroy_text(Text *text);

#endif
