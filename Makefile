#Library name
##Detect OS for the right extension
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Darwin)
	NAME	= SDL_wrapper.dylib
	SDL		= SDL2_macOS
	SDL_TTF	= SDL2_ttf_macOS
	SDL_IMG = SDL2_image_macOS
else
	NAME	= SDL_wrapper.so
	SDL		= SDL2_linux
endif

#Folders
DSRC 	= ./src/
DOBJ 	= ./obj/
DINC	= ./include
DLIB	= ./lib

#Compiler
CC		= gcc
FLAGS	= -Wall -pedantic -I $(DINC)

#Object files
OBJS	=	$(DOBJ)sdl_wrapper.o 	\
			$(DOBJ)sdl_ressources.o	\
			$(DOBJ)coord.o			\
			$(DOBJ)shape.o			\
			$(DOBJ)color.o			\
			$(DOBJ)text.o

#Rules
$(NAME): create_obj_dir $(OBJS)
	$(CC) -shared -o lib$@ $(OBJS) -L $(DLIB) -l$(SDL) -l$(SDL_TTF)
	mv lib$(NAME) $(DLIB)/lib$(NAME)

##Create the obj dir if needed
create_obj_dir:
	mkdir -p $(DOBJ)

## Generic rule to compile .c files into .o files
$(DOBJ)%.o: $(DSRC)%.c
	$(CC) -fPIC -c $< -ansi $(FLAGS) -o $@

test: $(NAME)
	gcc test.c -o test -L$(DLIB) -l$(SDL) -l$(SDL_TTF) -l$(SDL_IMG) -lSDL_wrapper -Wl,-rpath,$(DLIB)
	install_name_tool -change lib$(NAME) lib/lib$(NAME) test
	install_name_tool -change lib$(SDL_TTF) lib/lib$(SDL_TTF) test
	install_name_tool -change lib$(SDL_TTF) lib/lib$(SDL_IMG) test

# Cleaning rules
re: fclean $(NAME)

clean:
	rm -rf $(OBJS)

fclean: clean
	rm -f lib$(NAME)
	rm -f test

.PHONY: re clean fclean
